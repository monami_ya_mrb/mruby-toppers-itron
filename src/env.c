/*
** itron.c - API wrapper for ITRON
**
*/

#include "mruby.h"
#include "mruby/hash.h"
#include "mruby/khash.h"
#include "mruby/class.h"
#include "mruby/array.h"
#include "mruby/string.h"
#include "mruby/variable.h"
#include "itron.h"
#include "kernel.h"

static mrb_value
dly_tsk_(mrb_state *mrb, mrb_value self)
{
  mrb_int n;
  ER ercd;

  mrb_get_args(mrb, "i", &n);

  ercd = dly_tsk(n);

  return mrb_fixnum_value(ercd);
}

void
mrb_mruby_toppers_itron_gem_init(mrb_state *mrb)
{
  struct RClass *i;

  i = mrb_define_class(mrb, "ITRON", mrb->object_class);

  mrb_define_class_method(mrb, i, "dly_tsk",   dly_tsk_,   MRB_ARGS_REQ(1));
}

void
mrb_mruby_toppers_itron_gem_final(mrb_state *mrb)
{
}
